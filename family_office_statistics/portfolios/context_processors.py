# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from .models import Portfolio


def user_portfolio(request):
    queryset = Portfolio.objects.all()
    if request.user.is_authenticated() and queryset.filter(user=request.user).exists():
        portfolio = queryset.get(user=request.user)
    else:
        portfolio = None
    return {'user_portfolio': portfolio}
