from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import Portfolio


@admin.register(Portfolio)
class PortfolioAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Equities', {
            'classes': ('wide',),
            'fields': (
                ('lg_cap_US_E', 'md_cap_US_E', 'sm_cap_US_E'),
                ('lg_cap_non_US_E', 'md_cap_non_US_E', 'sm_cap_non_US_E'),
                ('emerging_mkt_E', 'frontier_mkt_E'),
                ('US_PE', 'non_US_PE')
            )
        }),
        ('Real Estate', {
            'classes': ('wide',),
            'fields': (
                ('US_real_estate', 'non_US_real_estate'),
                ('US_land', 'non_US_land'),
            )
        }),
        ('Bonds', {
            'classes': ('wide',),
            'fields': (
                ('US_corp_bonds', 'US_treasury_bonds', 'US_muni_bonds'),
                ('non_US_corp_bonds', 'non_US_gov_bonds'),
                ('dir_lending_global_bonds', 'emerging_mkt_bonds'),
            )
        }),
        ('Commodities', {
            'classes': ('wide',),
            'fields': ('gold', 'other_commodities')
        }),
        ('Venture Investments', {
            'classes': ('wide',),
            'fields': ('angel_investments', 'global_venture_cap')
        }),
        ('Other', {
            'classes': ('wide',),
            'fields': (
                ('cash', 'managed_futures', 'high_freq_trading'),
                ('collectibles', 'crypto_currencies', 'other'),
            )
        })
    )
    readonly_fields = (
        'lg_cap_US_E',
        'md_cap_US_E',
        'sm_cap_US_E',
        'lg_cap_non_US_E',
        'md_cap_non_US_E',
        'sm_cap_non_US_E',
        'emerging_mkt_E',
        'frontier_mkt_E',
        'US_PE',
        'non_US_PE',
        'US_real_estate',
        'non_US_real_estate',
        'US_land',
        'non_US_land',
        'US_corp_bonds',
        'US_treasury_bonds',
        'US_muni_bonds',
        'non_US_corp_bonds',
        'non_US_gov_bonds',
        'dir_lending_global_bonds',
        'emerging_mkt_bonds',
        'gold',
        'other_commodities',
        'angel_investments',
        'global_venture_cap',
        'cash',
        'managed_futures',
        'high_freq_trading',
        'collectibles',
        'crypto_currencies',
        'other'
    )