# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Portfolio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lg_cap_US_E', models.DecimalField(null=True, verbose_name='large-cap U.S. equities', max_digits=5, decimal_places=2)),
                ('md_cap_US_E', models.DecimalField(null=True, verbose_name='mid-cap U.S. equities', max_digits=5, decimal_places=2)),
                ('sm_cap_US_E', models.DecimalField(null=True, verbose_name='small-cap U.S. equities', max_digits=5, decimal_places=2)),
                ('lg_cap_non_US_E', models.DecimalField(null=True, verbose_name='large-cap non-U.S. equities', max_digits=5, decimal_places=2)),
                ('md_cap_non_US_E', models.DecimalField(null=True, verbose_name='mid-cap non-U.S. equities', max_digits=5, decimal_places=2)),
                ('sm_cap_non_US_E', models.DecimalField(null=True, verbose_name='small-cap non-U.S. equities', max_digits=5, decimal_places=2)),
                ('emerging_mkt_E', models.DecimalField(null=True, verbose_name='emerging market equities', max_digits=5, decimal_places=2)),
                ('frontier_mkt_E', models.DecimalField(null=True, verbose_name='frontier market equities', max_digits=5, decimal_places=2)),
                ('US_PE', models.DecimalField(null=True, verbose_name='U.S. private equities', max_digits=5, decimal_places=2)),
                ('non_US_PE', models.DecimalField(null=True, verbose_name='non-U.S. private equities', max_digits=5, decimal_places=2)),
                ('US_real_estate', models.DecimalField(null=True, verbose_name='U.S. real estate (exluding land)', max_digits=5, decimal_places=2)),
                ('non_US_real_estate', models.DecimalField(null=True, verbose_name='non-U.S. real estate (excluding land)', max_digits=5, decimal_places=2)),
                ('US_land', models.DecimalField(null=True, verbose_name='U.S. land', max_digits=5, decimal_places=2)),
                ('non_US_land', models.DecimalField(null=True, verbose_name='non-U.S. land', max_digits=5, decimal_places=2)),
                ('US_corp_bonds', models.DecimalField(null=True, verbose_name='U.S. corporate bonds', max_digits=5, decimal_places=2)),
                ('US_treasury_bonds', models.DecimalField(null=True, verbose_name='U.S. Treasury bonds', max_digits=5, decimal_places=2)),
                ('US_muni_bonds', models.DecimalField(null=True, verbose_name='U.S. municipal bonds', max_digits=5, decimal_places=2)),
                ('non_US_corp_bonds', models.DecimalField(null=True, verbose_name='non-U.S. corporate bonds', max_digits=5, decimal_places=2)),
                ('non_US_gov_bonds', models.DecimalField(null=True, verbose_name='non-U.S. government bonds', max_digits=5, decimal_places=2)),
                ('dir_lending_global_bonds', models.DecimalField(null=True, verbose_name='direct lending global bonds', max_digits=5, decimal_places=2)),
                ('emerging_mkt_bonds', models.DecimalField(null=True, verbose_name='emerging markets bonds', max_digits=5, decimal_places=2)),
                ('gold', models.DecimalField(null=True, verbose_name='gold', max_digits=5, decimal_places=2)),
                ('other_commodities', models.DecimalField(null=True, verbose_name='other commodities', max_digits=5, decimal_places=2)),
                ('angel_investments', models.DecimalField(null=True, verbose_name='angel investments', max_digits=5, decimal_places=2)),
                ('global_venture_cap', models.DecimalField(null=True, verbose_name='global venture capital', max_digits=5, decimal_places=2)),
                ('cash', models.DecimalField(null=True, verbose_name='cash', max_digits=5, decimal_places=2)),
                ('managed_futures', models.DecimalField(null=True, verbose_name='managed futures', max_digits=5, decimal_places=2)),
                ('high_freq_trading', models.DecimalField(null=True, verbose_name='high-frequency trading', max_digits=5, decimal_places=2)),
                ('collectibles', models.DecimalField(help_text='e.g. art', null=True, verbose_name='collectibles', max_digits=5, decimal_places=2)),
                ('crypto_currencies', models.DecimalField(help_text='e.g. Bitcoin', null=True, verbose_name='crypto-currencies', max_digits=5, decimal_places=2)),
                ('other', models.DecimalField(null=True, verbose_name='other', max_digits=5, decimal_places=2)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
