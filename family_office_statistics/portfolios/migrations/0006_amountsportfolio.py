# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
from decimal import Decimal
import portfolios.model_fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('portfolios', '0005_auto_20151111_1640'),
    ]

    operations = [
        migrations.CreateModel(
            name='AmountsPortfolio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lg_cap_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='large-cap U.S. equities', max_digits=15, decimal_places=2)),
                ('md_cap_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='mid-cap U.S. equities', max_digits=15, decimal_places=2)),
                ('sm_cap_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='small-cap U.S. equities', max_digits=15, decimal_places=2)),
                ('lg_cap_non_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='large-cap non-U.S. equities', max_digits=15, decimal_places=2)),
                ('md_cap_non_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='mid-cap non-U.S. equities', max_digits=15, decimal_places=2)),
                ('sm_cap_non_US_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='small-cap non-U.S. equities', max_digits=15, decimal_places=2)),
                ('emerging_mkt_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='emerging market equities', max_digits=15, decimal_places=2)),
                ('frontier_mkt_E', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='frontier market equities', max_digits=15, decimal_places=2)),
                ('US_PE', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. private equities', max_digits=15, decimal_places=2)),
                ('non_US_PE', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='non-U.S. private equities', max_digits=15, decimal_places=2)),
                ('US_real_estate', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. real estate (excluding land)', max_digits=15, decimal_places=2)),
                ('non_US_real_estate', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='non-U.S. real estate (excluding land)', max_digits=15, decimal_places=2)),
                ('US_land', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. land', max_digits=15, decimal_places=2)),
                ('non_US_land', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='non-U.S. land', max_digits=15, decimal_places=2)),
                ('US_corp_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. corporate bonds', max_digits=15, decimal_places=2)),
                ('US_treasury_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. Treasury bonds', max_digits=15, decimal_places=2)),
                ('US_muni_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='U.S. municipal bonds', max_digits=15, decimal_places=2)),
                ('non_US_corp_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='non-U.S. corporate bonds', max_digits=15, decimal_places=2)),
                ('non_US_gov_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='non-U.S. government bonds', max_digits=15, decimal_places=2)),
                ('dir_lending_global_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='direct lending global bonds', max_digits=15, decimal_places=2)),
                ('emerging_mkt_bonds', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='emerging markets bonds', max_digits=15, decimal_places=2)),
                ('gold', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='gold', max_digits=15, decimal_places=2)),
                ('other_commodities', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='other commodities', max_digits=15, decimal_places=2)),
                ('angel_investments', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='angel investments', max_digits=15, decimal_places=2)),
                ('global_venture_cap', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='global venture capital', max_digits=15, decimal_places=2)),
                ('cash', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='cash', max_digits=15, decimal_places=2)),
                ('managed_futures', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='managed futures', max_digits=15, decimal_places=2)),
                ('high_freq_trading', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='high-frequency trading', max_digits=15, decimal_places=2)),
                ('collectibles', portfolios.model_fields.AssetAmountField(default=Decimal('0'), help_text='e.g. art', verbose_name='collectibles', max_digits=15, decimal_places=2)),
                ('crypto_currencies', portfolios.model_fields.AssetAmountField(default=Decimal('0'), help_text='e.g. Bitcoin', verbose_name='crypto-currencies', max_digits=15, decimal_places=2)),
                ('other', portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name='other', max_digits=15, decimal_places=2)),
                ('user', models.OneToOneField(related_name='amounts_portfolio', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
