# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from decimal import Decimal
import portfolios.model_fields


class Migration(migrations.Migration):

    dependencies = [
        ('portfolios', '0006_amountsportfolio'),
    ]

    operations = [
        migrations.AddField(
            model_name='amountsportfolio',
            name='master_limited_partnership',
            field=portfolios.model_fields.AssetAmountField(default=Decimal('0'), verbose_name="MLP's", max_digits=15, decimal_places=2),
        ),
        migrations.AddField(
            model_name='portfolio',
            name='master_limited_partnership',
            field=portfolios.model_fields.AssetPercentField(default=Decimal('0'), verbose_name="MLP's", max_digits=5, decimal_places=2),
        ),
    ]
