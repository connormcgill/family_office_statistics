# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import include, url

from . import views


urlpatterns = [
    url(
        regex=r'^$',
        view=views.PortfolioView.as_view(),
        name="detail"
    ),
    url(r'^update/',
        include([
            url(
                regex=r'^percentages/$',
                view=views.PortfolioPercentagesUpdateView.as_view(),
                name="update_percentages"
            ),
            url(
                regex=r'^amounts/$',
                view=views.PortfolioAmountsUpdateView.as_view(),
                name="update_amounts"
            )
        ])
    ),
    # url(
    #     r'^create/',
    #     include([
    #         url(
    #             regex=r'^$',
    #             view=views.portfolio_wizard,
    #             name='wizard'
    #         ),
    #         url(
    #             regex=r'^(?P<step>[-\w]+)/$',
    #             view=views.portfolio_wizard,
    #             name='wizard_step'
    #         ),
    #     ])
    # ),
    url(r'^create/',
        include([
            url(
                regex=r'^percentages/$',
                view=views.PortfolioPercentagesCreateView.as_view(),
                name='create_percentages'
            ),
            url(
                regex=r'^amounts/$',
                view=views.PortfolioAmountsCreateView.as_view(),
                name='create_amounts'
            ),
            url(
                regex=r'^$',
                view=views.PortfolioCreationMethodChoiceView.as_view(),
                name='create'
            ),
        ])
    ),
    url(
        regex=r'^delete/$',
        view=views.PortfolioDeleteView.as_view(),
        name="delete"
    ),
]