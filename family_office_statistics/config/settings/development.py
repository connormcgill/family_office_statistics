"""Development settings and globals."""

from __future__ import absolute_import, unicode_literals
from os.path import join, normpath

from .common import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!-lbj+!p7w3ric9#h7i&!vv^xxtnbkbx-np1ol)yx6*kpt5w+v'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = DEBUG


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': normpath(join(CONFIG_DIR, 'development.sqlite3')),
    }
}


STATIC_ROOT = normpath(join(ROOT_DIR, 'assets', 'static'))
MEDIA_ROOT = normpath(join(ROOT_DIR, 'assets', 'uploads'))

ACCOUNT_EMAIL_CONFIRMATION_EMAIL = False
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = False
ACCOUNT_OPEN_SIGNUP = True


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''