# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.views.generic import TemplateView

from braces.views import UserPassesTestMixin

from portfolios.models import Portfolio, AmountsPortfolio

from django.shortcuts import render


class HomepageView(TemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        context = super(HomepageView, self).get_context_data(**kwargs)
        
        return context


#terms and privacy html pages
class PrivacyPolicyView(TemplateView):
	template_name = 'privacy_policy.html'

class TermsOfUseView(TemplateView):
	template_name = 'terms_of_use.html'

class ContactUsView(TemplateView):
	template_name = 'contact_us.html'

class RequestAnInviteView(TemplateView):
	template_name = 'request_an_invite.html'