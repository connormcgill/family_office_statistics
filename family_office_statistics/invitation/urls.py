from django.conf.urls import url, include
from .views import InvitationView

urlpatterns = [
    url(r'^$', InvitationView.as_view(), name='request_an_invite')
]
