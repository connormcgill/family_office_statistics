from __future__ import absolute_import, unicode_literals

from django import forms


class HTML5AutofocusMixin(object):
    """
    Mixin for adding the HTML `autofocus` attribute to the specified form
    field.
    """
    autofocus_field = ''

    def __init__(self, *args, **kwargs):
        super(HTML5AutofocusMixin, self).__init__(*args, **kwargs)
        if self.autofocus_field and self.autofocus_field in self.fields:
            self.fields[self.autofocus_field].widget.attrs['autofocus'] = 'autofocus'


class BootstrapFormMixin(object):
    """
    Mixin for adding Bootstrap markup (class="form-control") to the
    fields on a form.
    """
    def __init__(self, *args, **kwargs):
        super(BootstrapFormMixin, self).__init__()
        
        for field_name in self.Meta.fields:
            if not isinstance(self.fields[field_name].widget, forms.CheckboxInput):
                if 'class' in self.fields[field_name].widget.attrs:
                    self.fields[field_name].widget.attrs['class'] + ' form-control'
                else:
                    self.fields[field_name].widget.attrs['class'] = 'form-control'


class WidgetTweaksMixin(object):
    """
    Form mixin that allows for convenient manual addition of HTML
    attributes to a form widget.

    Attributes:
        `widget_tweaks`
            A dictionary mapping field names to sub-dictionaries of HTML
            attribute names and their values.
            
    """

    widget_tweaks = None

    def __init__(self, *args, **kwargs):
        super(WidgetTweaksMixin, self).__init__(*args, **kwargs)
        if self.widget_tweaks is not None:
            for label, attr_dict in self.widget_tweaks.items():
                for attr, value in attr_dict.items():
                    if attr == 'class' and attr in self.fields[label].widget.attrs:
                        self.fields[label].widget.attrs[attr] += ' {}'.format(value)
                    else:
                        self.fields[label].widget.attrs[attr] = value
